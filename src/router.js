import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import store from "./store.js";
Vue.use(Router);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return;
    }
    next("/");
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated && ((store.getters.getType === "administrator") || (store.getters.getType == "professor") || (store.getters.getType === "student"))) {
        next();
        return;
    }
    next("/");
};



// const isAdministrator = (to, from, next) => {
//     if (store.getters.getType == "administrator") {
//         next();
//         return;
//     }
//     from();
//     // next("/");
// };
//
// const isProfessor = (to, from, next) => {
//     if (store.getters.getType == "professor") {
//         next();
//         return;
//     }
//     from();
//     // next("/");
// };
//
// const isStudent = (to, from, next) => {
//     if (store.getters.getType == "student") {
//         next();
//         return;
//     }
//     from();
//     // next("/");
// };

export default new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "home",
            component: Home
        },
        {
            path: "/courses/:key",
            name: "course",
            component: () =>
                import(/* webpackChunkName: "course" */ "./views/Course.vue"),
            beforeEnter: ifAuthenticated
        },
        {
            path: "/addCourse",
            name: "newCourse",
            component: () =>
                import(/* webpackChunkName: "newCourse" */ "./views/NewCourse.vue"),
            beforeEnter: ifAuthenticated
        },
        {
            path: "/courses/:key/addForm",
            name: "newForm",
            component: () =>
                import(/* webpackChunkName: "newForm" */ "./views/NewForm.vue"),
            beforeEnter: ifAuthenticated
        },
        {
            path: "/courses/:id/polls/:key/answer",
            name: "answerForm",
            component: () =>
                import(/* webpackChunkName: "answerForm" */ "./views/AnswerForm.vue"),
            beforeEnter: ifAuthenticated
        },
        {
            path: "/authorization",
            name: "authorization",
            component: () =>
                import(/* webpackChunkName: "answerForm" */ "./views/Authorization.vue"),
            beforeEnter: ifNotAuthenticated
        },
        {
            path: "/registration",
            name: "registration",
            component: () =>
                import(/* webpackChunkName: "answerForm" */ "./views/Registration.vue"),
            beforeEnter: ifNotAuthenticated
        },
        {
            path: "/courses/:id/feedback/:key",
            name: "results",
            component: () =>
                import(/* webpackChunkName: "answerForm" */ "./views/ResultsOfForm.vue"),
            beforeEnter: ifAuthenticated
        },
        {
            path: "/temp",
            name: "temp",
            component: () =>
                import(/* webpackChunkName: "answerForm" */ "./views/temp.vue"),
        },
        {
            path: "/courses/:key/addTemplate",
            name: "newTemplate",
            component: () =>
                import(/* webpackChunkName: "newTemplate" */ "./views/NewTemplate.vue"),
            beforeEnter: ifAuthenticated
        },
        {
            path: "/courses/:key/templates/:id",
            name: "templateView",
            component: () =>
                import(/* webpackChunkName: "answerForm" */ "./views/TemplateView.vue"),
            beforeEnter: ifAuthenticated
        },
        {
            path: "/courses/:key/addForm/:id",
            name: "newForm",
            component: () =>
                import(/* webpackChunkName: "newForm" */ "./views/NewForm.vue"),
            beforeEnter: ifAuthenticated
        },
    ]
});
