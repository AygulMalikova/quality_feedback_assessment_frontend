// import {AUTH_ERROR, AUTH_LOGOUT, AUTH_REQUEST, AUTH_SUCCESS} from "./store/actions/auth";
// import axios from "axios";
// import {USER_REQUEST} from "./store/actions/user";
// import {postAuthRequest} from "./services/auth";
//
// const state = { token: localStorage.getItem('user-token') || '',
//   status: localStorage.getItem('status') || ''}
//
// const getters = {
//   isAuthenticated: state => !!state.token,
//   authStatus: state => state.status,
// }
//
// const actions = {
//   [AUTH_REQUEST]: ({commit, dispatch}, {username, password} ) => {
//     return new Promise((resolve, reject) => {
//       commit(AUTH_REQUEST)
//       //console.log(username, password)
//       postAuthRequest(username, password)
//       // apiCall({url: 'auth', data: user, method: 'POST'})
//           .then(response => {
//             // localStorage.setItem('user-token', response.token)
//             // console.log(response.data.token)
//             commit(AUTH_SUCCESS, response.data)
//             axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('user-token')
//             dispatch(USER_REQUEST)
//             resolve(response)
//           })
//           .catch(error => {
//             console.log(error);
//             commit(AUTH_ERROR, error)
//             localStorage.removeItem('user-token')
//             reject(error)
//           })
//     })
//   },
//   [AUTH_LOGOUT]: ({commit, dispatch}) => {
//     return new Promise((resolve, reject) => {
//       commit(AUTH_LOGOUT)
//       //console.log(localStorage.getItem('user-token'))
//       resolve()
//     })
//   }
// }
//
// const mutations = {
//   [AUTH_REQUEST]: (state) => {
//     state.status = 'loading'
//     localStorage.setItem("status", state.status)
//   },
//   [AUTH_SUCCESS]: (state, resp) => {
//     state.status = 'success'
//     state.token = resp.token
//     localStorage.setItem("user-token", state.token)
//     localStorage.setItem("status", state.status)
//     // console.log(state.token)
//     state.hasLoadedOnce = true
//   },
//   [AUTH_ERROR]: (state) => {
//     state.status = 'error'
//     localStorage.setItem("status", state.status)
//     state.hasLoadedOnce = true
//   },
//   [AUTH_LOGOUT]: (state) => {
//     state.token = ''
//     localStorage.removeItem('user-token')
//   }
// }
//
// export default {
//   state,
//   getters,
//   actions,
//   mutations,
// }
