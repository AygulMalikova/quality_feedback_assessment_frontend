import Vue from "vue";
import Vuex from "vuex";
import {
    AUTH_ERROR,
    AUTH_LOGOUT,
    AUTH_REQUEST,
    AUTH_SUCCESS, SET_TYPE
} from "./store/actions/auth";
import axios from "axios";
import { USER_REQUEST } from "./store/actions/user";
import { postAuthRequest } from "./services/auth";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        token: localStorage.getItem("user-token") || "",
        status: localStorage.getItem("status") || "",
        email: localStorage.getItem("email") || "",
        // type: "student"
        type: localStorage.getItem("user-type") || ""
    },
    mutations: {
        [AUTH_REQUEST]: (state, email) => {
            state.status = "loading";
            localStorage.setItem("status", state.status);
            state.email = email;
            localStorage.setItem("email", state.email);
        },
        [AUTH_SUCCESS]: (state, resp) => {
            state.status = "success";
            localStorage.setItem("status", state.status);
            state.token = resp.token;
            localStorage.setItem("user-token", state.token);
            //state.type = resp.type;
            // localStorage.setItem("user-type", state.type);
            state.hasLoadedOnce = true;
        },
        [AUTH_ERROR]: state => {
            state.status = "error";
            localStorage.setItem("status", state.status);
            state.hasLoadedOnce = true;
            state.email = "";
            localStorage.setItem("email", state.email);
        },
        [AUTH_LOGOUT]: state => {
            state.token = "";
            localStorage.removeItem("user-token");
            state.email = "";
            localStorage.removeItem("email");
            // state.type = "";
            // localStorage.removeItem("user-type");
            state.status = "logout";
            localStorage.setItem("status", state.status);
        },
        [SET_TYPE]: (state, type) => {
            state.type = type;
            // console.log("TYPE" + state.type)
            localStorage.setItem('user-type', state.type);
        }
        // [REFRESH_TOKEN]: (state, token) => {
        //     state.token = token;
        //     localStorage.setItem("user-token", state.token);
        // }
    },
    actions: {
        [AUTH_REQUEST]: ({ commit, dispatch }, { username, password }) => {
            return new Promise((resolve, reject) => {
                commit(AUTH_REQUEST, username);
                //console.log(username, password)
                postAuthRequest(username, password)
                    // apiCall({url: 'auth', data: user, method: 'POST'})
                    .then(response => {
                        // localStorage.setItem('user-token', response.token)
                        // console.log(response.data.token)
                        commit(AUTH_SUCCESS, response.data);
                        axios.defaults.headers.common["Authorization"] =
                            "Bearer " + localStorage.getItem("user-token");
                        dispatch(USER_REQUEST);
                        resolve(response);
                    })
                    .catch(error => {
                        // console.log(error);
                        commit(AUTH_ERROR, error);
                        reject(error);
                    });
            });
        },
        [AUTH_LOGOUT]: ({ commit, dispatch }, ) => {
            return new Promise((resolve, reject) => {
                commit(AUTH_LOGOUT);
                //console.log(localStorage.getItem('user-token'))
                resolve();
            });
        },
        [SET_TYPE]: ({ commit, dispatch }, type) => {
            return new Promise((resolve, reject) => {
                commit(SET_TYPE, type);
                resolve();
            });
        }
        // [REFRESH_TOKEN]: ({ commit, dispatch }, token) => {
        //     return new Promise((resolve, reject) => {
        //         commit(REFRESH_TOKEN, token);
        //         resolve();
        //     });
        // }
    },
    getters: {
        isAuthenticated: state => {
            return !!state.token;
        },
        authStatus: state => {
            return state.status;
        },
        getToken: state => {
            return state.token;
        },
        getEmail: state => {
            return state.email;
        },
        getType: state => {
            return state.type;
        }
    }
});

export default store;
