import axios from "axios";

const urlPrefix = process.env.VUE_APP_API_ENDPOINT_URL;

// let config = {
//   headers: {
//     Authorization: "Bearer " + store.getters.getToken()
//   }
// }

export const fetchCourses = () => {
    // console.log(localStorage.getItem('user-token'))
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.get(`${urlPrefix}/courses/`);
};

export const fetchCourse = key => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.get(`${urlPrefix}/courses/${key}/`);
};

export const postPost = (name, prof, ta, img, date_from, date_to) => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.post(`${urlPrefix}/courses/add/`, {
        name,
        prof,
        ta,
        img,
        date_from,
        date_to
    });
};
