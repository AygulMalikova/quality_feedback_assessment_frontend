import axios from "axios";
import store from "../store.js";

const urlPrefix = process.env.VUE_APP_API_ENDPOINT_URL;

export const postAuthRequest = (username, password) => {
    return axios.post(`${urlPrefix}/auth/obtain_token/`, {
    username, password
    });
};


export const postRegisterData = (username, password) => {
    // `${urlPrefix}/users/register/`
    return axios.post(`${urlPrefix}/users/register/`, {
        username, password
    });
};

export const getUserTypeBD = () => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    // `${urlPrefix}/users/register/`
    return axios.get(`${urlPrefix}/users/type/`, store.getters.getToken);
};

// // const token = store.getters.getToken;
//
// export const refreshToken = (token) => {
//     return axios.post(`${urlPrefix}/auth/refresh_token/`, {token});
// }