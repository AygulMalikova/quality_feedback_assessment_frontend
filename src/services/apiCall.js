const mocks = {
    auth: { POST: { token: "" } },
    "user/me": { GET: { name: "doggo", title: "sir" } }
};

const apiCall = ({ url, method, ...args }) =>
    new Promise((resolve, reject) => {
        setTimeout(() => {
            try {
                resolve(mocks[url][method || "GET"]);
                console.log(`Mocked '${url}' - ${method || "GET"}`);
                console.log("response: ", mocks[url][method || "GET"]);
            } catch (error) {
                reject(new Error(error));
            }
        }, 1000);
    });

export default apiCall;
