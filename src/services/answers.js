import axios from "axios";

const urlPrefix = process.env.VUE_APP_API_ENDPOINT_URL;

export const postAnswers = (course, key, answers) => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.post(`${urlPrefix}/courses/${course}/polls/${key}/answer/`, {
        key,
        answers
    });
};

export const fetchAnswer = (course, key) => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.get(`${urlPrefix}/courses/${course}/feedback/${key}/`);
};

export const fetchAnswerByFormId = (course, form) => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.get(
        `${urlPrefix}/courses/${course}/feedback_by_form/${form}/`
    );
};

export const fetchAnswers = course => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.get(`${urlPrefix}/courses/${course}/feedback/`);
};
