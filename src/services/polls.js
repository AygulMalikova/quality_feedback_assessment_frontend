import axios from "axios";

const urlPrefix = process.env.VUE_APP_API_ENDPOINT_URL;

export const fetchForms = key => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.get(`${urlPrefix}/courses/${key}/polls/`);
};

export const fetchTemplates = () => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.get(`${urlPrefix}/templates/`);
};

export const fetchPoll = (course_id, poll_id) => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.get(`${urlPrefix}/courses/${course_id}/polls/${poll_id}/`);
};

export const fetchTemplate = (template_id) => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.get(`${urlPrefix}/templates/${template_id}`);
};

export const addForm = (
    course,
    type,
    week,
    topic,
    link,
    availability,
    questions
) => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    return axios.post(`${urlPrefix}/courses/${course}/polls/add/`, {
        course,
        type,
        week,
        topic,
        link,
        availability,
        questions
    });
};

export const addTemplate = (
    course,
    name,
    type,
    questions,
    otherCourses
) => {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("user-token");
    if (!otherCourses) {
        course = "";
    }
    return axios.post(`${urlPrefix}/templates/`, {
        course,
        name,
        type,
        questions
    });
};

